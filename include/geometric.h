#ifndef GEOMETRIC_H_INCLUDED
#define GEOMETRIC_H_INCLUDED
#pragma once

#include <stdint.h>
#include <math.h>
#include <cinder/Vector.h>

using namespace cinder;

// A collection of useful 2D and 3D geometric constructs.

namespace Geo
{
	static const float K_FLOAT_EPSILON = 1.0e-5f;
	static const float K_FLOAT_MIN = -1.0e+37f;
	static const float K_FLOAT_MAX = 1.0e+37f;

	enum IntersectionType
	{
		IT_None,
		IT_Parallel,
		IT_Coincident,
		IT_Intersect,
		IT_Enclosed
	};

	inline Vec2f perpLeft(const Vec2f& a)
	{
		return Vec2f(-a.x, a.y);
	}

	inline Vec2f perpRight(const Vec2f& a)
	{
		return Vec2f(a.x, -a.y);
	}

	inline float cross(const Vec2f& a, const Vec2f& b)
	{
		return a.x * b.y - a.y * b.x;	
	}
	

	class LineSegment
	{
	public:
		LineSegment(const Vec2f& begin, const Vec2f& end)
			: mBegin(begin)
			, mEnd(end)
		{
		}
		
		const Vec2f& begin() const { return mBegin; }
		const Vec2f& end() const { return mEnd; }
		Vec2f vector() const { return mEnd - mBegin; }
		Vec2f direction() const { return vector().normalized(); }
		float length() const { return vector().length(); }
        float lengthSquared() const { return vector().lengthSquared(); }

		void reverse()
		{
			Vec2f swap = mEnd;
			mEnd = mBegin;
			mBegin = swap;
		}
		
		Vec2f thetaPoint(float theta) const
		{
			return lerp(mBegin, mEnd, theta);
		}
		
		float parallelDistance(const Vec2f& point) const
		{
			return dot(direction(), point - mBegin);
		}

		float closestTheta(const Vec2f& point) const;
		Vec2f closestPoint(const Vec2f& point) const;
		float distance(const Vec2f& point) const;
		float distanceSquared(const Vec2f& point) const;

		IntersectionType testIntersection(const LineSegment& other, float& tau) const;

	private:
		Vec2f mBegin;
		Vec2f mEnd;
	};

	class Ray
	{
	public:
		Ray(Vec2f origin, Vec2f direction)
		{
			mOrigin = origin;
			mDirection = direction;
		}
		
		const Vec2f& origin() const { return mOrigin; }
		const Vec2f& direction() const { return mDirection; }

		IntersectionType testIntersection(const LineSegment& lineSegment, float& distance) const;

	private:
		Vec2f mOrigin;
		Vec2f mDirection;
	};

	class AlignedRect
	{
	public:
		AlignedRect()
			: mMin(1.0e9f, 1.0e9f)
			, mMax(-1.0e9f, -1.0e9f)
		{
		}
		
		AlignedRect(const Vec2f& min, const Vec2f& max)
			: mMin(min)
			, mMax(max)
		{
		}
		
		const Vec2f& min() const { return mMin; }
		const Vec2f& max() const { return mMax; }
		Vec2f centre() const { return (mMin + mMax) * 0.5f; }
		Vec2f extents() const { return (mMin - mMax) * 0.5f; }
        Vec2f x0y0() const { return mMin; }
        Vec2f x1y0() const { return Vec2f(mMax.x, mMin.y); }
        Vec2f x0y1() const { return Vec2f(mMin.x, mMax.y); }
        Vec2f x1y1() const { return mMax; }
		
		void include(const Vec2f& point);
        void include(float x, float y);
        void include(const AlignedRect& rect);
		
		bool testInside(const Vec2f& point) const
		{
			return (point.x >= mMin.x && point.y >= mMin.y && point.x <= mMax.x && point.y <= mMax.y);
		}
		
		static AlignedRect intersect(const AlignedRect& a, const AlignedRect& b);
        IntersectionType testIntersection(const Ray& ray, float& distance) const;
        IntersectionType testIntersection(const LineSegment& line, float& t) const;

	private:
        static bool raySlabIntersect(float min, float max, float rayStart, float rayEnd, float& tEnter, float& tExit);

		Vec2f mMin;
		Vec2f mMax;
	};
}

#endif
