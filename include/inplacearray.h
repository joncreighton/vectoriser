#ifndef INPLACEARRAY_H_INCLUDED
#define INPLACEARRAY_H_INCLUDED
#pragma once

#include <stdint.h>

template< typename T, uint32_t CAPACITY >
class InPlaceArray
{
public:
	InPlaceArray()
		: mSize(0)
	{
	}

	uint32_t size() const { return mSize; }
	uint32_t capacity() const { return CAPACITY; }
	T* data() { return mData; }
	const T* data() const { return mData; }
	T& operator[](uint32_t idx) { return mData[idx]; }
	const T& operator[](uint32_t idx) const { return mData[idx]; }

	void push_back(const T& val)
	{
		assert(mSize < CAPACITY);
		mData[mSize++] = val;
	}

	void remove(uint32_t idx)
	{
		for (uint32_t i = idx; i < mSize - 1; ++i)
		{
			mData[i] = mData[i + 1];
		}

		--mSize;
	}

	int32_t find(const T& val) const
	{
		for (uint32_t i = 0; i < mSize; ++i)
		{
			if (mData[i] == val)
				return i;
		}

		return -1;
	}

private:
	T mData[CAPACITY];
	uint32_t mSize;
};

#endif // INPLACEARRAY_H_INCLUDED