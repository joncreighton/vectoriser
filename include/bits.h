#ifndef UTILITIES_H_INCLUDED
#define UTILITIES_H_INCLUDED
#pragma once

#include <stdint.h>

namespace Bits
{
	inline uint8_t getByte(uint32_t word, uint32_t i)
    {
        return static_cast< uint8_t >((word >> (i * 8)) & 0xFF);
    }

    inline uint32_t setByte(uint32_t word, uint32_t i, uint8_t val)
    {
        uint32_t mask = 0xFFFFFFFFUL ^ (0xFF << (i * 8));
        return word & mask | (static_cast< uint32_t >(val) << (i * 8));
    }
	
	inline int32_t modInc(int32_t val, int32_t begin, int32_t end)
    {
        ++val;
        return val - ((end - begin) & (((end - 1) - val) >> 31));
    }

    inline int32_t modDec(int32_t val, int32_t begin, int32_t end)
    {
        --val;
        return val + ((end - begin) & ((val - begin) >> 31));
    }

    inline int32_t modInc(int32_t val, int32_t end)
    {
        ++val;
        return val - (end & (((end - 1) - val) >> 31));
    }

    inline int32_t modDec(int32_t val, int32_t end)
    {
        --val;
        return val + (end & (val >> 31));
    }

    inline int32_t modInc(int32_t val, int32_t step, int32_t begin, int32_t end)
    {
        val += step;
        return val - ((end - begin) & (((end - 1) - val) >> 31));
    }

    inline int32_t modDec(int32_t val, int32_t step, int32_t begin, int32_t end)
    {
        val -= step;
        return val + ((end - begin) & ((val - begin) >> 31));
    }
}

#endif // UTILITIES_H_INCLUDED
