#ifndef VECTORISER_POLY_H_INCLUDED
#define VECTORISER_POLY_H_INCLUDED
#pragma once

#include <stdint.h>
#include <vector>
#include <map>

#include "cinder/Vector.h"
#include "cinder/Surface.h"

#include "bits.h"
#include "geometric.h"
#include "inplacearray.h"

using namespace cinder;

namespace cinder
{
	class Shape2d;
}

namespace Vectoriser
{
	class Edge
	{
	public:
		Edge()
			: mX(0)
			, mY(0)
			, mPairPoly(0)
			, mPairEdge(0)
			, mContourID(0)
		{
		}

		Edge(int32_t x, int32_t y, uint32_t contourID, bool important)
			: mX(x)
			, mY(y)
			, mPairPoly(0)
			, mPairEdge(0)
			, mContourID((contourID << 1) | (important ? 1 : 0))
		{
		}

		bool isCoincident(const Edge& other) const
		{
			return (mX == other.mX && mY == other.mY);
		}

		void set(int32_t x, int32_t y, uint32_t contourID, bool important)
		{
			mX = x;
			mY = y;
			mPairPoly = 0;
			mPairEdge = 0;
			mContourID = (contourID << 1) | (important ? 1 : 0);
		}

		uint64_t posHash() const
		{
			return (static_cast<uint64_t>(mY) << 32) | (static_cast<uint64_t>(mX) & 0xFFFFFFFFUL);
		}

		bool isImportant() const
		{
			return mContourID & 0x1;
		}

		uint32_t contourID() const
		{
			return mContourID >> 1;
		}

		Vec2f pos(float invScale)
		{
			return Vec2f(static_cast< float >(mX), static_cast< float >(mY)) * invScale;
		}

		int32_t mX;
		int32_t mY;
		uint32_t mContourID;	// Lower bit is whether it's important or not.
		int32_t mPairPoly;
		int32_t mPairEdge;
	};

	class Contour
	{
	public:
		Contour()
			: mBegin(-1)
			, mEnd(-1)
		{
		}

		int32_t mBegin;
		int32_t mEnd;
		Geo::AlignedRect mBounds;
	};

	typedef std::vector< Edge > EdgeVector;
	typedef std::vector< Vec2f > VecVector;
	typedef std::vector< Contour > ContourVector;
	typedef InPlaceArray< uint64_t, 4 > VertTable;

	class Poly
	{
	public:
		Poly(uint32_t val, int32_t startX, int32_t startY, int32_t startCell);

		uint32_t value() const								{ return mValue; }
		bool isIsland() const								{ return mIsIsland; }
		void setIsIsland(bool isIsland)						{ mIsIsland = isIsland; }
		uint32_t triangleCount() const						{ return mEdges.size() + (2 * mContours.size()) - 4; }
		const EdgeVector& edges() const						{ return mEdges; }
		EdgeVector& edges()									{ return mEdges; }
		const VecVector& vertices() const					{ return mVertices; }
		VecVector& vertices()								{ return mVertices; }
		const ContourVector& contours() const				{ return mContours; }
		ContourVector& contours()							{ return mContours; }

		uint32_t vertContourID(int32_t vertIdx) const		{ return mEdges[vertIdx].mContourID >> 1; }
		const Contour& vertContour(int32_t vertIdx) const	{ return mContours[vertContourID(vertIdx)]; }
		int32_t pairVert(int32_t vertIdx)					{ return mEdges[vertIdx].mPairEdge; }
		int32_t pairPoly(int32_t vertIdx)					{ return mEdges[vertIdx].mPairPoly; }

		// Get the next/prev vert in a contour, wrapping around at the end/begin.
		int32_t nextVert(int32_t vertIdx) const
		{
			const Contour& contour = vertContour(vertIdx);
			return Bits::modInc(vertIdx, contour.mBegin, contour.mEnd);
		}

		int32_t prevVert(int32_t vertIdx) const
		{
			const Contour& contour = vertContour(vertIdx);
			return Bits::modDec(vertIdx, contour.mBegin, contour.mEnd);
		}

		// Return the line segment that defines an edge starting from vertex vertIdx.
		Geo::LineSegment edgeLine(int32_t vertIdx) const
		{
			int32_t nextIdx = nextVert(vertIdx);
			return Geo::LineSegment(mVertices[vertIdx], mVertices[nextIdx]);
		}

		int32_t countVerts(int32_t beginIdx, int32_t endIdx) const
		{
			const Contour& contour = vertContour(beginIdx);
			return (endIdx > beginIdx) ? endIdx - beginIdx : (contour.mEnd - beginIdx) + (endIdx - contour.mBegin);
		}

		void allocateVertices()
		{
			mVertices.resize(mEdges.size());
		}

		void convertToShape(Shape2d& shape) const;

		bool testInside(const Vec2f& pos) const;
		bool testInsideContour(const Contour& contour, const Vec2f& pos) const;
		void nearestEdge(const Vec2f& pos, int32_t& nearestVert, float& minDistanceToVert, int32_t& nearestEdge, float& minDistanceToEdge) const;
		Geo::IntersectionType testIntersection(const Geo::Ray& ray, int32_t& edgeIdx, float& distance) const;
		Geo::IntersectionType testIntersection(const Geo::LineSegment& line, int32_t& edgeIdx, float& distance) const;

	private:
		uint32_t mValue;
		int32_t mStartX;
		int32_t mStartY;
		int32_t mStartCell;
		bool mIsIsland;
		ContourVector mContours;
		EdgeVector mEdges;
		VecVector mVertices;
	};

	typedef std::vector< Poly > PolyVector;
}

#endif // VECTORISER_POLY_H_INCLUDED