#ifndef VECTORISER_MESH_H_INCLUDED
#define VECTORISER_MESH_H_INCLUDED
#pragma once

#include <stdint.h>
#include <vector>
#include <map>

#include "cinder/Vector.h"
#include "cinder/Surface.h"

#include "bits.h"
#include "geometric.h"
#include "inplacearray.h"
#include "poly.h"

using namespace cinder;

namespace Vectoriser
{
	class Mesh
	{
	public:
		Mesh();
		void process(const Surface8u& surface);
		PolyVector& polygons() { return mPolygons; }
		const PolyVector& polygons() const { return mPolygons; }

		int32_t subdivideEdge(int32_t polyID, int32_t vertIdx, int32_t numPoints);
		int32_t removeVert(int32_t polyID, int32_t vertIdx);
		void moveVert(int32_t polyID, int32_t vertIdx, Vec2f newPos);
		bool removePolygon(int32_t polyID);
		void smoothPolygon(int32_t polyID, float smoothing);

		void posToMap(Vec2f planarPos, int32_t& x, int32_t& y) const;
		Vec2f mapToPos(int32_t x, int32_t y) const;

	private:
		void removePairEntry(int32_t polyID, int32_t vertIdx);
		void trimVert(int32_t polyID, int32_t vertIdx);
		bool colinear(int32_t x0, int32_t y0, const Edge& e1, const Edge& e2) const;
		void newVert(int32_t polyID, int32_t x, int32_t y, int32_t pointIdx, bool important);
		bool testInside(const Poly& poly, int32_t x, int32_t y) const;
		bool testInside(const Poly& poly, const Contour& contour, float x, float y) const;
		bool getQuad(const Surface8u& surface, int32_t x, int32_t y, uint32_t& val0, uint32_t& val1, uint32_t& val2, uint32_t& val3) const;
		void traceContour(const Surface8u& surface, uint8_t visited[], int32_t x, int32_t y, uint32_t polyValue, int32_t cell);
		void insertVertsInternal(int32_t polyID, int32_t vertIdx, int32_t numPoints, int32_t skipVert);
		void removeVertInternal(int32_t polyID, int32_t vertIdx);
		void prevTJuncVert(int32_t polyID, int32_t vertIdx, int32_t& pairPolyID, int32_t& pairVertIdx) const;
		void nextTJuncVert(int32_t polyID, int32_t vertIdx, int32_t& pairPolyID, int32_t& pairVertIdx) const;
		void finalizePolygon(int32_t polyID);

		typedef std::map< uint64_t, VertTable > VertMap;

		// Const parameters.
		int32_t mMapWidth;
		int32_t mMapHeight;
		int32_t mQuadWidth;
		int32_t mQuadHeight;
		uint32_t mTotalVertexCount;
		bool mInitialised;
		Edge mContourHead;

		// Fixed allocation arrays.
		PolyVector mPolygons;

		// Table to find pair vertices.
		VertMap mPairLookup;
	};
}

#endif // VECTORISER_MESH_H_INCLUDED