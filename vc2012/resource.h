#pragma once
#include "cinder/CinderResources.h"

#define RES_PASSTHRU_VERT	CINDER_RESOURCE( ../resources/, passThru_vert.glsl, 128, GLSL )
#define RES_POSTERISE_FRAG	CINDER_RESOURCE( ../resources/, posterise_frag.glsl, 129, GLSL )
