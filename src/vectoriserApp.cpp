#include "cinder/app/AppNative.h"

#include "cinder/ArcBall.h"
#include "cinder/Camera.h"
#include "cinder/ImageIo.h"
#include "cinder/Rand.h"
#include "cinder/Surface.h"
#include "cinder/Triangulate.h"
#include "cinder/TriMesh.h"

#include "cinder/gl/gl.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/Vbo.h"

#include "Resources.h"
#include "vectoriser/mesh.h"

#include <vector>
#include <map>

using namespace ci;
using namespace ci::app;

class VectoriserApp : public AppNative {
public:
	void setup();
	void resize();
	void mouseDown( MouseEvent event );
	void mouseDrag( MouseEvent event );
	void update();
	void draw();
	void openFile();

private:
	void renderSceneToFBO();

	typedef std::map< uint32_t, TriMesh2d > TriMeshMap;

	CameraPersp	mCam;
	Arcball		mArcball;

	uint32_t	mWidth, mHeight;

	Matrix44f					mTorusRotation;

	Surface8u					mImage;
	gl::Fbo						mFBO;
	std::vector< gl::VboMesh >	mVBOMeshes;
	gl::GlslProgRef				mPosteriseShader;
	Vectoriser::Mesh			mVectoriser;
};

void VectoriserApp::setup()
{
	gl::enableAlphaBlending();
	gl::enableDepthRead();
	gl::enableDepthWrite();    

	mArcball.setQuat( Quatf( Vec3f( 0.0577576f, -0.956794f, 0.284971f ), 3.68f ) );

	try 
	{
		mPosteriseShader = gl::GlslProg::create( loadResource( RES_PASSTHRU_VERT ), loadResource( RES_POSTERISE_FRAG ) );
	}
	catch( gl::GlslProgCompileExc &exc )
	{
		std::cout << "Shader compile error: " << std::endl;
		std::cout << exc.what();
	}
	catch( ... )
	{
		std::cout << "Unable to load shader" << std::endl;
	}

	openFile();
}
void VectoriserApp::resize()
{
	mArcball.setWindowSize( getWindowSize() );
	mArcball.setCenter( Vec2f( getWindowWidth() / 2.0f, getWindowHeight() / 2.0f ) );
	mArcball.setRadius( getWindowHeight() / 2.0f );

/*
	mCam.lookAt( Vec3f( 0.0f, 0.0f, -150 ), Vec3f::zero() );
	mCam.setPerspective( 60.0f, getWindowAspectRatio(), 0.1f, 1000.0f );
	gl::setMatrices( mCam );
*/
}

void VectoriserApp::mouseDown( MouseEvent event )
{
    mArcball.mouseDown( event.getPos() );
}

void VectoriserApp::mouseDrag( MouseEvent event )
{
    mArcball.mouseDrag( event.getPos() );
}

void VectoriserApp::update()
{
}

void VectoriserApp::draw()
{
	Color colours[4] = { Color(0.8f, 0.4f, 0.0f), Color(0.4f, 0.8f, 0.0f), Color(0.4f, 0.0f, 0.8f), Color(0.4f, 0.8f, 0.0f) }; 

	gl::clear();

	gl::clear();
	gl::pushModelView();
	gl::translate( getWindowCenter() * Vec2f( 0.8f, 1.2f ) );
	gl::scale( Vec3f( 1.0f, 1.0f, 1.0f ) );
	gl::color( Color( 0.8f, 0.4f, 0.0f ) );

	uint32_t col = 0;

	for (std::vector<gl::VboMesh>::const_iterator it = mVBOMeshes.begin(); it != mVBOMeshes.end(); ++it)
	{
		gl::color( colours[col] );
		gl::draw( *it );

		col = (col + 1) % 4;
	}

	if (true)
	{
		gl::enableWireframe();
		gl::color( Color::white() );

		for (std::vector<gl::VboMesh>::const_iterator it = mVBOMeshes.begin(); it != mVBOMeshes.end(); ++it)
		{
			gl::draw( *it );
		}

		gl::disableWireframe();
	}

	gl::popModelView();

/*

	mTexture->enableAndBind();
	mPosteriseShader->bind();
	mPosteriseShader->uniform( "tex0", 0 );
	gl::drawSolidRect( getWindowBounds() );
*/
}


void VectoriserApp::openFile()
{
	fs::path path = getOpenFilePath("", ImageIo::getLoadExtensions());
	gl::TextureRef texture;

	if (!path.empty())
	{
		try {
			mImage = loadImage(path);
			texture = gl::Texture::create(mImage);

		}
		catch( ... ) {
			std::cout << "unable to load the texture file!" << std::endl;
		}

		if (texture != NULL)
		{
			mVectoriser.process(mImage);

			for (uint32_t i = 0; i < mVectoriser.polygons().size(); ++i)
			{
				mVectoriser.smoothPolygon(i, 0.5f);
			}

			
			TriMeshMap triMeshes;

			for (Vectoriser::PolyVector::const_iterator polyIt = mVectoriser.polygons().begin(), end = mVectoriser.polygons().end(); polyIt != end; ++polyIt)
			{
				Shape2d shape;

				polyIt->convertToShape(shape);

				TriMesh2d mesh = Triangulator(shape, 1.0f).calcMesh(Triangulator::WINDING_ODD);

				TriMeshMap::iterator meshIt = triMeshes.find(polyIt->value());

				if (meshIt != triMeshes.end())
				{
					uint32_t vertOffset = meshIt->second.getNumVertices();
					meshIt->second.appendVertices(&(mesh.getVertices()[0]), mesh.getVertices().size());

					for (uint32_t i = 0; i < mesh.getNumIndices(); i += 3)
					{
						uint32_t i0 = mesh.getIndices()[i + 0] + vertOffset;
						uint32_t i1 = mesh.getIndices()[i + 1] + vertOffset;
						uint32_t i2 = mesh.getIndices()[i + 2] + vertOffset;

						meshIt->second.appendTriangle(i0, i1, i2);
					}
				}
				else
				{
					triMeshes.insert(std::pair<uint32_t, TriMesh2d>(polyIt->value(), mesh));
				}
			}

			for (TriMeshMap::const_iterator mapIt = triMeshes.begin(), end = triMeshes.end(); mapIt != end; ++mapIt)
			{
				mVBOMeshes.push_back(gl::VboMesh(mapIt->second));
			}
		}
	}
}

void VectoriserApp::renderSceneToFBO()
{
	// this will restore the old framebuffer binding when we leave this function
	gl::SaveFramebufferBinding bindingSaver;
	
	// bind the framebuffer - now everything we draw will go there
	mFBO.bindFramebuffer();

	// setup the viewport to match the dimensions of the FBO
	gl::setViewport( mFBO.getBounds() );

	// setup our camera to render the torus scene
	CameraPersp cam( mFBO.getWidth(), mFBO.getHeight(), 60.0f );
	cam.setPerspective( 60, mFBO.getAspectRatio(), 1, 1000 );
	cam.lookAt( Vec3f( 2.8f, 1.8f, -2.8f ), Vec3f::zero() );
	gl::setMatrices( cam );

	// set the modelview matrix to reflect our current rotation
	gl::multModelView( mTorusRotation );
	
	// clear out the FBO with blue
	gl::clear( Color( 0.25, 0.5f, 1.0f ) );

	// render an orange torus, with no textures
	glDisable( GL_TEXTURE_2D );
	gl::color( Color( 1.0f, 0.5f, 0.25f ) );
	gl::drawTorus( 1.4f, 0.3f, 32, 64 );
//	gl::drawColorCube( Vec3f::zero(), Vec3f( 2.2f, 2.2f, 2.2f ) );
}

CINDER_APP_NATIVE( VectoriserApp, RendererGl )
