#include "geometric.h"

namespace Geo
{
	float LineSegment::closestTheta(const Vec2f& point) const
	{
		float distance = parallelDistance(point);
			
		if (distance < 0.0f)
		{
			return 0.0f;
		}
			
		float len = length();
		if (distance < len)
		{
			return (distance / len);
		}
			
		return 1.0f;
	}
		
	Vec2f LineSegment::closestPoint(const Vec2f& point) const
	{
		return thetaPoint(closestTheta(point));
	}
		
	float LineSegment::distance(const Vec2f& point) const
	{
		return (closestPoint(point) - point).length();
	}

    float LineSegment::distanceSquared(const Vec2f& point) const
    {
        return (closestPoint(point) - point).lengthSquared();
    }

    IntersectionType LineSegment::testIntersection(const LineSegment& other, float& tau) const
	{
		Vec2f thisDir = mEnd - mBegin;
		Vec2f otherDir = other.mEnd - other.mBegin;
		Vec2f thisDirNormalized = thisDir;
		Vec2f otherDirNormalized = otherDir;
			
		thisDirNormalized.normalize();
		otherDirNormalized.normalize();
			
		float d = dot(thisDirNormalized, otherDirNormalized);
			
		if (1.0f - fabsf(d) > K_FLOAT_EPSILON)
		{
			float c = cross(thisDir, otherDir);
				
			if (fabsf(c) > K_FLOAT_EPSILON)
			{
				Vec2f vecToOther = mBegin - other.mBegin;
				float r = cross(otherDir, vecToOther) / c;
				float s = cross(thisDir, vecToOther) / c;
					
				if (r >= 0.0f && s >= 0.0f && r <= 1.0f && s <= 1.0f)
				{
					thisDir.normalize();
					tau = r;
						
					return IT_Intersect;
				}
			}
		}
			
		tau = 0.0f;
		return IT_None;
	}

	IntersectionType Ray::testIntersection(const LineSegment& lineSegment, float& distance) const
	{
		Vec2f lineDir = lineSegment.vector();
		Vec2f normalizedLineDir = lineDir;
			
		normalizedLineDir.normalize();
			
		float d = dot(mDirection, normalizedLineDir);
			
		if (1.0f - fabsf(d) > K_FLOAT_EPSILON)
		{
			float c = cross(mDirection, lineDir);
				
			if (fabsf(c) > K_FLOAT_EPSILON)
			{
				Vec2f vecToLine = mOrigin - lineSegment.begin();
				float r = cross(lineDir, vecToLine) / c;
				float s = cross(mDirection, vecToLine) / c;
					
				if (r >= 0.0f && s >= 0.0f && s <= 1.0f)
				{
					distance = r;
						
					return IT_Intersect;
				}
			}
		}
			
		distance = -1.0f;
		return IT_None;
	}

	void AlignedRect::include(const Vec2f& point)
	{
		mMin.x = ( point.x < mMin.x ) ? point.x : mMin.x;
		mMin.y = ( point.y < mMin.y ) ? point.y : mMin.y;
		mMax.x = ( point.x > mMax.x ) ? point.x : mMax.x;
		mMax.y = ( point.y > mMax.y ) ? point.y : mMax.y;
	}

	void AlignedRect::include(float x, float y)
    {
        mMin.x = (x < mMin.x) ? x : mMin.x;
        mMin.y = (y < mMin.y) ? y : mMin.y;
        mMax.x = (x > mMax.x) ? x : mMax.x;
        mMax.y = (y > mMax.y) ? y : mMax.y;
    }

	void AlignedRect::include(const AlignedRect& rect)
	{
		mMin.x = ( rect.mMin.x < mMin.x ) ? rect.mMin.x : mMin.x;
		mMin.y = ( rect.mMin.y < mMin.y ) ? rect.mMin.y : mMin.y;
		mMax.x = ( rect.mMax.x > mMax.x ) ? rect.mMax.x : mMax.x;
		mMax.y = ( rect.mMax.y > mMax.y ) ? rect.mMax.y : mMax.y;
	}
		
	AlignedRect AlignedRect::intersect(const AlignedRect& a, const AlignedRect& b)
	{
		bool intersects = ( b.mMin.x <= a.mMax.x && 
			                b.mMax.x >= a.mMin.x && 
			                b.mMin.y <= a.mMax.y &&
			                b.mMax.y >= a.mMin.y ); 
			
		AlignedRect result;
			
		if (intersects) 
		{
			float x0 = (a.mMin.x > b.mMin.x) ? a.mMin.x : b.mMin.x;
			float y0 = (a.mMin.y > b.mMin.y) ? a.mMin.y : b.mMin.y;
			float x1 = (a.mMax.x < b.mMax.x) ? a.mMax.x : b.mMax.x;
			float y1 = (a.mMax.x < b.mMax.x) ? a.mMax.x : b.mMax.x;
			result.mMin = Vec2f( x0, y0 ); 
			result.mMax = Vec2f( x1, y1 );
		}
		else
		{
			result.mMin = Vec2f(0.0f, 0.0f); 
			result.mMax = Vec2f(0.0f, 0.0f); 
		}
			
		return result;
	}

    bool AlignedRect::raySlabIntersect(float min, float max, float rayStart, float rayEnd, float& tEnter, float& tExit)
    {
	    float rayDir = rayEnd - rayStart;

	    // ray parallel to the slab
	    if (fabsf(rayDir) < K_FLOAT_EPSILON)
	    {
		    // ray parallel to the slab, but ray not inside the slab planes
            if (rayStart < min || rayStart > max)
		    {
			    return false;
		    }
		    // ray parallel to the slab, but ray inside the slab planes
		    else
		    {
			    return true;
		    }
	    }

	    // slab's enter and exit parameters
        float tsEnter = (min - rayStart) / rayDir;
        float tsExit = (max - rayStart) / rayDir;

	    // order the enter / exit values.
	    if(tsEnter > tsExit)
	    {
            float temp = tsEnter;
            tsEnter = tsExit;
            tsExit = temp;
	    }

	    // make sure the slab interval and the current box intersection interval overlap
        if (tEnter > tsExit || tsEnter > tExit)
	    {
		    // nope. Ray missed the box.
		    return false;
	    }
	    // yep, the slab and current intersection interval overlap
	    else
	    {
		    // update the intersection interval
            tEnter = std::max(tEnter, tsEnter);
            tsExit = std::min(tsExit, tsExit);
		    return true;
	    }
    }

    IntersectionType AlignedRect::testIntersection(const Ray& ray, float& distance) const
    {
        bool inside = testInside(ray.origin());
        Vec2f tMin;
        Vec2f tMax;
        float invDirX = 1.0f / ray.direction().x;
        float invDirY = 1.0f / ray.direction().y;

        if (ray.direction().x >= 0.0f)
        {
            tMin.x = (mMin.x - ray.origin().x) * invDirX;
            tMax.x = (mMax.x - ray.origin().x) * invDirX;
        }
        else
        {
            tMin.x = (mMax.x - ray.origin().x) * invDirX;
            tMax.x = (mMin.x - ray.origin().x) * invDirX;
        }

        if (ray.direction().y >= 0.0f)
        {
            tMin.y = (mMin.y - ray.origin().y) * invDirY;
            tMax.y = (mMax.y - ray.origin().y) * invDirY;
        }
        else
        {
            tMin.y = (mMax.y - ray.origin().y) * invDirY;
            tMax.y = (mMin.y - ray.origin().y) * invDirY;
        }

        if ((tMin.x > tMax.y) || (tMin.y > tMax.x))
        {
            distance = -1.0f;
            return IT_None;
        }

        if (inside)
        {
            distance = std::min(tMax.y, tMax.x);
            return IT_Intersect;
        }
        else
        {
            distance = std::max(tMin.y, tMin.x);
            return (distance > 0.0f) ? IT_Intersect : IT_None;
        }
    }

    IntersectionType AlignedRect::testIntersection(const LineSegment& line, float& t) const
    {
        bool inside0 = testInside(line.begin());
        bool inside1 = testInside(line.end());

        if (inside0 && inside1)
        {
            t = -1.0f;
            return IT_Enclosed;
        }

        Vec2f tMin;
        Vec2f tMax;
        Vec2f direction = line.vector();
        float invDirX = 1.0f / direction.x;
        float invDirY = 1.0f / direction.y;

        if (direction.x >= 0.0f)
        {
            tMin.x = (mMin.x - line.begin().x) * invDirX;
            tMax.x = (mMax.x - line.begin().x) * invDirX;
        }
        else
        {
            tMin.x = (mMax.x - line.begin().x) * invDirX;
            tMax.x = (mMin.x - line.begin().x) * invDirX;
        }

        if (direction.y >= 0.0f)
        {
            tMin.y = (mMin.y - line.begin().y) * invDirY;
            tMax.y = (mMax.y - line.begin().y) * invDirY;
        }
        else
        {
            tMin.y = (mMax.y - line.begin().y) * invDirY;
            tMax.y = (mMin.y - line.begin().y) * invDirY;
        }

        if ((tMin.x > tMax.y) || (tMin.y > tMax.x))
        {
            t = -1.0f;
            return IT_None;
        }

        if (inside0)
        {
            t = std::min(tMax.y, tMax.x);
            return IT_Intersect;
        }
        else
        {
            t = std::max(tMin.y, tMin.x);
            return (t > 0.0f) ? IT_Intersect : IT_None;
        }
    }
}
