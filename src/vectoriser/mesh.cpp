#include "vectoriser/mesh.h"

namespace Vectoriser
{
// Lookup tables.

// The order of the cells in the quad is:
// +-----+
// | 2 3 |
// | 0 1 |
// +-----+

// The direction of the approach to the quad determines which cell to test:
// +-----+
// | E S |
// | N W |
// +-----+

//
// This results the following lines:
//
// 0        1        2        3        4        5        6        7
// O-----O  O-----O  O-----O  O-----O  #-----O  #-----O  #-----O  #-----O
// |     |  |     |  |     |  |_____|  |/    |  |  |  |  |    \|  |    \|
// |     |  |\    |  |    /|  |     |  |     |  |  |  |  |\    |  |     |
// O-----O  #-----O  O-----#  #-----#  O-----O  #-----O  O-----#  #-----#
//
// 8        9        A        B        C        D        E        F
// O-----#  #-----O  O-----#  O-----#  #-----#  #-----#  #-----#  #-----#
// |    \|  |/    |  |  |  |  |/    |  |_____|  |     |  |     |  |     |
// |     |  |    /|  |  |  |  |     |  |     |  |    /|  |\    |  |     |
// O-----O  O-----#  O-----#  #-----#  O-----O  #-----O  O-----#  #-----#
//                                  0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
static const int32_t sCorner[] = { -1,  1,  3,  1,  0,  0, -1,  0,  2, -1,  3,  1,  2,  2,  3, -1 };
static const int32_t sPoint0[] = {  0,  1,  2,  2,  3,  1,  0,  2,  4,  0,  4,  4,  3,  1,  3,  0 };
static const int32_t sPoint1[] = {  0,  3,  1,  3,  4,  4,  0,  4,  2,  0,  1,  3,  2,  2,  1,  0 };

static const int32_t sDeltaX[] = { 0, -1, 1, 0 };
static const int32_t sDeltaY[] = { 1, 0, 0, -1 };

//
// There are three 'complex' quads that introduce more than a single line into contours:
// +-----+ +-----+ +-----+
// | 6 9 | | 4 9 | | 6 8 |
// | 9 6 | | 9 2 | | 1 6 |
// +-----+ +-----+ +-----+

static const uint32_t sDoubleLines[][4] =
{
	{ 13, 2, 4, 11 },		// 0x9669
	{ 13, 2, 4, 11 },		// 0x9429
	{ 1, 7, 14, 8 },		// 0x8661
};


// These are the five possible points for lines in the quad, relative to the centre.
static const int32_t sPoints[][2] =
{
	{ 0,  0},
	{ 0, -1},
	{ 1,  0},
	{-1,  0},
	{ 0,  1},
};

inline uint64_t hashVertIdx(int32_t polyID, int32_t vertIdx)
{
	return (static_cast<uint64_t>(polyID) << 32) | (static_cast<uint64_t>(vertIdx) & 0xFFFFFFFF);
}

Mesh::Mesh()
	: mTotalVertexCount(0)
	, mInitialised(false)
{
}
void Mesh::removePairEntry(int32_t polyID, int32_t vertIdx)
{
    uint64_t posHash = mPolygons[polyID].edges()[vertIdx].posHash();
    VertMap::iterator it = mPairLookup.find(posHash);

    if (it != mPairLookup.end())
    {
		uint64_t idxHash = hashVertIdx(polyID, vertIdx);
		int32_t idx = it->second.find(idxHash);

		if (idx != -1)
		{
			it->second.remove(idx);
		}
    }
}

void Mesh::trimVert(int32_t polyID, int32_t vertIdx)
{
    Poly& poly = mPolygons[polyID];
    removePairEntry(polyID, vertIdx);
    Contour& contour = poly.contours().back();

	// For all of the vertices that are further along in the contour from the one that we're removing
	// we have to find their entries in the pair lookup map and shift their indexes down by 1.
    for (int32_t i = vertIdx + 1; i < contour.mEnd; ++i)
    {
        uint64_t posHash = poly.edges()[i].posHash();
		VertMap::iterator it = mPairLookup.find(posHash);

        if (it != mPairLookup.end())
        {
			uint64_t idxHash = hashVertIdx(polyID, i);
			int32_t idx = it->second.find(idxHash);

			assert(idx != -1);

			--(it->second[idx]);
        }

        poly.edges()[i - 1] = poly.edges()[i];
    }

    --contour.mEnd;
    --mTotalVertexCount;
    poly.edges().pop_back();
}

bool Mesh::colinear(int32_t x0, int32_t y0, const Edge& e1, const Edge& e2) const
{
    int32_t x0x1 = x0 - e1.mX;
    int32_t x0x2 = x0 - e2.mX;
    int32_t y0y1 = y0 - e1.mY;
    int32_t y0y2 = y0 - e2.mY;

    return (y0y1 * x0x2 == y0y2 * x0x1);
}

void Mesh::newVert(int32_t polyID, int32_t x, int32_t y, int32_t pointIdx, bool important)
{
    Poly& poly = mPolygons[polyID];
    int32_t posX = x * 2 + 1 + sPoints[pointIdx][0];
    int32_t posY = y * 2 + 1 + sPoints[pointIdx][1];
    int32_t contourIdx = poly.contours().size() - 1;
    Contour& contour = poly.contours()[contourIdx];

    if (contour.mBegin == -1)
    {
        contour.mBegin = poly.edges().size();
        contour.mEnd = contour.mBegin;
    }
    else
    {
        int32_t vertCount = contour.mEnd - contour.mBegin;
        int32_t prevVert = contour.mEnd - 1;

        if (vertCount == 0 || mContourHead.isImportant() || !colinear(posX, posY, mContourHead, poly.edges()[prevVert]))
        {
            uint64_t posHash = mContourHead.posHash();
			VertMap::iterator it = mPairLookup.find(posHash);

			if (it == mPairLookup.end())
			{
				it = mPairLookup.insert(std::pair<uint64_t, VertTable>(posHash, VertTable())).first;
			}

			uint64_t idxHash = hashVertIdx(polyID, contour.mEnd);
			it->second.push_back(idxHash);
			
			contour.mBounds.include(mContourHead.pos(1.0f));

			poly.edges().push_back(mContourHead);
            ++mTotalVertexCount;
            ++contour.mEnd;
        }
    }

    // Update the contour head to the new position.
    mContourHead.set(posX, posY, contourIdx, important);
}

bool Mesh::testInside(const Poly& poly, int32_t x, int32_t y) const
{
    float px = x * 2.0f + 1.0f;
    float py = y * 2.0f + 1.0f;
    bool inside = false;
	ContourVector::const_iterator it = poly.contours().begin();

    if (testInside(poly, *it, px, py))
    {
        inside = true;
		++it;

        while(it != poly.contours().end())
        {
            if (testInside(poly, *it, px, py))
            {
                inside = false;
                break;
            }

			++it;
        }
    }

    return inside;
}

bool Mesh::testInside(const Poly& poly, const Contour& contour, float x, float y) const
{
    bool inside = false;

    if (contour.mBounds.testInside(Vec2f(x, y)))
    {
        int32_t beginVert = contour.mBegin;
        int32_t endVert = contour.mEnd;

        if (endVert - beginVert > 0)
        {
            for (int32_t v1 = beginVert, v0 = endVert - 1; v1 < endVert; v0 = v1++)
            {
                float x0 = static_cast< float >(poly.edges()[v0].mX);
                float y0 = static_cast< float >(poly.edges()[v0].mY);
                float x1 = static_cast< float >(poly.edges()[v1].mX);
                float y1 = static_cast< float >(poly.edges()[v1].mY);
                float dx = x1 - x0;
                float dy = y1 - y0;

                // From http://local.wasp.uwa.edu.au/~pbourke/geometry/insidepoly/

                if (((y0 <= y) && (y < y1)) || ((y1 <= y) && (y < y0)))
                {
                    if ((x - x1) < dx * (y - y1) / dy)
                    {
                        inside = !inside;
                    }
                }
            }
        }
    }

    return inside;
}

bool Mesh::getQuad(const Surface8u& surface, int32_t x, int32_t y, uint32_t& val0, uint32_t& val1, uint32_t& val2, uint32_t& val3) const
{
	bool xEdge = false;
	bool yEdge = false;

	val0 = 0xFF000000UL;
	val1 = 0xFF000000UL;
	val2 = 0xFF000000UL;
	val3 = 0xFF000000UL;

	if (y >= 0)
	{
		const uint8_t* line0 = surface.getData(Vec2i(x, y));

		if (x >= 0)
			val0 = *reinterpret_cast< const uint32_t* >(line0) & 0xFFFFFFUL;
		else
			xEdge = true;

		if (x < (int)mMapWidth - 1)
			val1 = *reinterpret_cast< const uint32_t* >(line0 + surface.getPixelInc()) & 0xFFFFFFUL;
		else
			xEdge = true;
	}
	else
	{
		yEdge = true;
	}

	if (y < (int)mMapHeight - 1)
	{
		const uint8_t* line1 = surface.getDataRed(Vec2i(x, y + 1));

		if (x >= 0)
			val2 = *reinterpret_cast< const uint32_t* >(line1) & 0xFFFFFFUL;
		else
			xEdge = true;

		if (x < (int)mMapWidth - 1)
			val3 = *reinterpret_cast< const uint32_t* >(line1 + surface.getPixelInc()) & 0xFFFFFFUL;
		else
			xEdge = true;
	}
	else
	{
		yEdge = true;
	}

	// If the edgeCount was 2 then we're in a corner.
	return xEdge && yEdge;
}

inline uint32_t calcLine(uint32_t testValue, uint32_t val0, uint32_t val1, uint32_t val2, uint32_t val3)
{
    uint32_t lineVal = (((val0 == testValue) ? 1 : 0)
                      | ((val1 == testValue) ? 2 : 0)
                      | ((val2 == testValue) ? 4 : 0)
                      | ((val3 == testValue) ? 8 : 0));

    return lineVal;
}

void Mesh::traceContour(const Surface8u& surface, uint8_t visited[], int32_t x, int32_t y, uint32_t polyValue, int32_t cell)
{
	uint32_t val0, val1, val2, val3;
	uint32_t lines[4];
	int32_t polyID = -1;

	for (uint32_t i = 0; i < mPolygons.size(); ++i)
	{
		if (mPolygons[i].value() == polyValue && testInside(mPolygons[i], x, y))
		{
			polyID = i;
			break;
		}
	}

	if (polyID != -1)
	{
		mPolygons[polyID].contours().push_back(Contour());
	}
	else
	{
		polyID = mPolygons.size();
		mPolygons.push_back(Poly(polyValue, x, y, cell));
	}

	Poly& polygon = mPolygons[polyID];
	uint32_t line = 0;
	bool isVert0 = true;
	int32_t quadIdx = (x + 1) + (y + 1) * mQuadWidth;

	do
	{
		bool isCorner = getQuad(surface, x, y, val0, val1, val2, val3);

		lines[0] = calcLine(val0, val0, val1, val2, val3);
		lines[1] = calcLine(val1, val0, val1, val2, val3);
		lines[2] = calcLine(val2, val0, val1, val2, val3);
		lines[3] = calcLine(val3, val0, val1, val2, val3);

		uint32_t lineHash = lines[0] | lines[1] << 4 | lines[2] << 8 | lines[3] << 12;

		// For an explanation of what these magic numbers mean, check the comments at the top of this file.
		int32_t doubleID = (lineHash == 0x9669UL) ? 0 :
						   (lineHash == 0x9429UL) ? 1 :
						   (lineHash == 0x8661UL) ? 2 : -1;
		bool isTJunct = false;

		if (doubleID == -1)
		{
			isTJunct = (lineHash == 0xCC21UL) ||
					   (lineHash == 0x8433UL) ||
					   (lineHash == 0x8525UL) ||
					   (lineHash == 0xA4A1UL) ||
					   (lineHash == 0x8421UL);

			line = lines[cell];
			visited[quadIdx] |= line;
		}
		else
		{
			line = sDoubleLines[doubleID][cell];
			visited[quadIdx] |= (uint8_t)(1 << cell);
		}

		if (isVert0) newVert(polyID, x, y, sPoint0[line], false);
		if (isTJunct || isCorner) newVert(polyID, x, y, 0, true);
		newVert(polyID, x, y, sPoint1[line], false);

		isVert0 = false;
		cell = sCorner[line];
		x += sDeltaX[cell];
		y += sDeltaY[cell];
		quadIdx = (x + 1) + (y + 1) * mQuadWidth;
	}
	while ((visited[quadIdx] & (uint8_t)(1 << cell)) == 0);

	// Remove the last vert that was added if it's the same as the first in the contour.
	Contour& contour = polygon.contours().back();
	int32_t firstIdx = contour.mBegin;
	int32_t lastIdx = contour.mEnd - 1;

	if (!mContourHead.isCoincident(polygon.edges()[firstIdx]))
	{
		polygon.edges().push_back(mContourHead);
		++mTotalVertexCount;
		++contour.mEnd;
		++lastIdx;
	}

	// Check whether the first vert is colinear between the second and last ones.  If so, remove it.
	if (colinear(polygon.edges()[firstIdx].mX, polygon.edges()[firstIdx].mY, polygon.edges()[lastIdx], polygon.edges()[firstIdx + 1]))
	{
		trimVert(polyID, firstIdx);
	}
}

void Mesh::insertVertsInternal(int32_t polyID, int32_t vertIdx, int32_t numPoints, int32_t skipVert)
{
    Poly& poly = mPolygons[polyID];
    int32_t oldCount = poly.edges().size();

    for (int32_t i = vertIdx + skipVert; i < oldCount; ++i)
    {
        // Fix all of the _pairID vertices that reference verts in this poly past vertIdx.
        int32_t pairVert = poly.edges()[i].mPairEdge;

		if (pairVert != -1)
        {
            Poly& pairPoly = mPolygons[poly.edges()[i].mPairPoly];
            pairPoly.edges()[pairVert].mPairEdge += numPoints;
        }
    }

    poly.vertices().resize(poly.vertices().size() + numPoints);

    for (int32_t i = oldCount - 1; i > vertIdx; --i)
    {
        // Shift all verts from (_currVert - 1) to (vertIdx + 1) up by numPoints.
        poly.edges()[i + numPoints] = poly.edges()[i];
        poly.vertices()[i + numPoints] = poly.vertices()[i];
    }

    // Fix up all the contours so that their mBeginVert and mEndVert values are correct.
	for (ContourVector::iterator it = poly.contours().begin(); it != poly.contours().end(); ++it)
    {
        it->mBegin += (it->mBegin > vertIdx) ? numPoints : 0;
        it->mEnd += (it->mEnd > vertIdx) ? numPoints : 0;
    }
}

// Get the next vert that shares the same t-junction.
void Mesh::prevTJuncVert(int32_t polyID, int32_t vertIdx, int32_t& pairPolyID, int32_t& pairVertIdx) const
{
    const Poly& poly = mPolygons[polyID];
    const Edge& edge = poly.edges()[poly.prevVert(vertIdx)];
    pairPolyID = edge.mPairPoly;
    pairVertIdx = -1;

    if (poly.edges()[vertIdx].isImportant() && pairPolyID != -1)
    {
        pairVertIdx = edge.mPairEdge;
    }
}

// Get the prev vert that shares the same t-junction.
void Mesh::nextTJuncVert(int32_t polyID, int32_t vertIdx, int32_t& pairPolyID, int32_t& pairVertIdx) const
{
    const Edge& edge = mPolygons[polyID].edges()[vertIdx];
    pairPolyID = edge.mPairPoly;
    pairVertIdx = -1;

    if (edge.isImportant() && pairPolyID != -1)
    {
        pairVertIdx = mPolygons[pairPolyID].nextVert(edge.mPairEdge);
    }
}

// Add points on the edge that begins from vertIdx
int32_t Mesh::subdivideEdge(int32_t polyID, int32_t vertIdx, int32_t numPoints)
{
    if (numPoints == 0)
        return 0;

    Poly& poly = mPolygons[polyID];
    int32_t nextID = poly.nextVert(vertIdx);
    int32_t contourID = poly.edges()[vertIdx].mContourID & 0xFFFE;
    int32_t pairVertIdx = poly.edges()[vertIdx].mPairEdge;
    int32_t pairPolyID = poly.edges()[vertIdx].mPairPoly;
    Vec2f v0 = poly.vertices()[vertIdx];
    Vec2f v1 = poly.vertices()[nextID];

    insertVertsInternal(polyID, vertIdx, numPoints, 1);

    for (int32_t i = 0; i < numPoints; ++i)
    {
        int32_t id = vertIdx + i + 1;
        poly.edges()[id].mContourID = contourID;
        poly.edges()[id].mPairPoly = pairPolyID;
        poly.edges()[id].mPairEdge = ((pairVertIdx != -1) ? pairVertIdx + numPoints - i - 1 : -1);
        poly.vertices()[id] = (v0 + ((v1 - v0) * (i + 1)) / static_cast<float>(numPoints + 1));
    }

    if (pairVertIdx != -1)
    {
        Poly& pairPoly = mPolygons[pairPolyID];
        int32_t pairContourID = pairPoly.edges()[pairVertIdx].mContourID & 0xFFFE;

        insertVertsInternal(pairPolyID, pairVertIdx, numPoints, 0);

        pairPoly.edges()[pairVertIdx].mPairEdge += numPoints;

        for (int32_t i = 0; i < numPoints; ++i)
        {
            int32_t pairID = pairVertIdx + numPoints - i;
            int32_t id = vertIdx + i;
            pairPoly.edges()[pairID].mContourID = pairContourID;
            pairPoly.edges()[pairID].mPairEdge = id;
            pairPoly.edges()[pairID].mPairPoly = polyID;
            pairPoly.vertices()[pairID] = poly.vertices()[id + 1];
        }
    }

    mTotalVertexCount += numPoints * 2;

    return numPoints;
}

void Mesh::removeVertInternal(int32_t polyID, int32_t vertIdx)
{
    Poly& poly = mPolygons[polyID];

    // Fix up all the contours so that their mBeginVert and mEndVert values are correct.
	for (ContourVector::iterator it = poly.contours().begin(); it != poly.contours().end(); ++it)
    {
        it->mBegin -= (it->mBegin > vertIdx) ? 1 : 0;
        it->mEnd -= (it->mEnd > vertIdx) ? 1 : 0;
    }

	int32_t vertCount = poly.edges().size();

    for (int32_t i = vertIdx; i < vertCount; ++i)
    {
        // Fix all of the _pairID vertices that reference verts in this poly past vertIdx.
        int32_t pairVert = poly.edges()[i].mPairEdge;
        if (pairVert != -1)
        {
            Poly pairPoly = mPolygons[poly.edges()[i].mPairPoly];

            pairPoly.edges()[pairVert].mPairEdge = poly.prevVert(i);
        }
    }

    for (int32_t i = vertIdx; i < vertCount - 1; ++i)
    {
        // Shift all verts from (_currVert - 1) to (vertIdx + 1) up by numPoints.
        poly.edges()[i] = poly.edges()[i + 1];
        poly.vertices()[i] = poly.vertices()[i + 1];
    }

    poly.edges().pop_back();
    poly.vertices().pop_back();
}

// Remove the vert from its contour as well as any pairing ones.
int32_t Mesh::removeVert(int32_t polyID, int32_t vertIdx)
{
    Poly& poly = mPolygons[polyID];

    if (poly.edges().size() < 4 || poly.edges()[vertIdx].isImportant())
        return 0;

    int32_t pairVertIdx = poly.edges()[vertIdx].mPairEdge;
    int32_t pairPolyID = poly.edges()[vertIdx].mPairPoly;

    if (pairPolyID != -1 && mPolygons[pairPolyID].edges().size() < 4)
        return 0;

    removeVertInternal(polyID, vertIdx);

    if (pairPolyID != -1)
    {
        int32_t twinVertIdx = mPolygons[pairPolyID].nextVert(pairVertIdx);
        removeVertInternal(pairPolyID, twinVertIdx);
    }

    mTotalVertexCount -= 2;

    return 1;
}

void Mesh::moveVert(int32_t polyID, int32_t vertIdx, Vec2f newPos)
{
    Poly& poly = mPolygons[polyID];

    if (poly.edges()[vertIdx].isImportant())
    {
        // This is a good pattern to use for iterating through all the verts in a t-junction.  
        int32_t pairPolyID = poly.edges()[vertIdx].mPairPoly;
        int32_t thisPolyID = polyID;

        poly.vertices()[vertIdx] = newPos;

        while (pairPolyID != polyID && pairPolyID != -1)
        {
            Poly& thisPoly = mPolygons[thisPolyID];
            Poly& pairPoly = mPolygons[pairPolyID];

            vertIdx = pairPoly.nextVert(thisPoly.edges()[vertIdx].mPairEdge);
            pairPoly.vertices()[vertIdx] = newPos;

            thisPolyID = pairPolyID;
            pairPolyID = pairPoly.edges()[vertIdx].mPairPoly;
        }
    }
    else
    {
        poly.vertices()[vertIdx] = newPos;

        if (poly.edges()[vertIdx].mPairEdge != -1)
        {
            Poly& pairPoly = mPolygons[poly.edges()[vertIdx].mPairPoly];
            int32_t twinVert = pairPoly.nextVert(poly.edges()[vertIdx].mPairEdge);

            pairPoly.vertices()[twinVert] = newPos;
        }
    }
}

bool Mesh::removePolygon(int32_t polyID)
{
    Poly& poly = mPolygons[polyID];

    if (!poly.isIsland())
        return false;

    // Recursively remove any polygons that are fully inclosed within this one.
    for (uint32_t contourID = 1; contourID < poly.contours().size(); ++contourID)
    {
        // We only need to check the neighbour of the first vert.
        const Contour& contour = poly.contours()[contourID];
        int32_t childPolyID = poly.edges()[contour.mBegin].mPairPoly;

        if (childPolyID != -1)
        {
            if (mPolygons[childPolyID].isIsland())
            {
                removePolygon(childPolyID);
            }
        }
    }

    int32_t parentPolyID = poly.edges().begin()->mPairPoly;

    if (parentPolyID == -1)
        return false;

    Poly& parentPoly = mPolygons[parentPolyID];
    const Edge& parentEdge = parentPoly.edges()[poly.edges()[0].mPairEdge];
    uint32_t parentContourID = parentEdge.contourID();
    const Contour& parentContour = parentPoly.contours()[parentContourID];
    int32_t vertCount = parentContour.mEnd - parentContour.mBegin;

    // Remove the contour from the parent's polygon.
    for (uint32_t contourID = parentContourID + 1; contourID < parentPoly.contours().size(); ++contourID)
    {
        parentPoly.contours()[contourID].mBegin -= vertCount;
        parentPoly.contours()[contourID].mEnd -= vertCount;
    }

    // Now fix up all of the pair references in the vertices.
	uint32_t parentEdgeCount = parentPoly.edges().size();
    for (uint32_t vertIdx = parentContour.mEnd; vertIdx < parentEdgeCount; ++vertIdx)
    {
        // Move the verts back.
        int newVertIdx = vertIdx - vertCount;
        parentPoly.edges()[newVertIdx] = parentPoly.edges()[vertIdx];
        parentPoly.edges()[newVertIdx].mContourID -= 2;		// Decrement the contour index part of the contour ID (remember that bit 0 is the 'important' flag).
        parentPoly.vertices()[newVertIdx] = parentPoly.vertices()[vertIdx];
    }

	// Trim off the now-unused tails of the edge and vertice arrays.
    parentPoly.edges().resize(parentEdgeCount - vertCount);
    parentPoly.vertices().resize(parentEdgeCount - vertCount);

	// Remove the contour from the polygon and the polygon from the mesh.
    parentPoly.contours().erase(parentPoly.contours().begin() + parentContourID);
    mPolygons.erase(mPolygons.begin() + parentContourID);

    for (uint32_t otherPolyID = 0; otherPolyID < mPolygons.size(); ++otherPolyID)
    {
        Poly& otherPoly = mPolygons[otherPolyID];

        for (uint32_t otherVertID = 0; otherVertID < otherPoly.edges().size(); ++otherVertID)
        {
			// Fix up the references to the edges in the parent polygon in the rest of the mesh.
            if (otherPoly.edges()[otherVertID].mPairPoly == parentPolyID)
            {
                if (otherPoly.edges()[otherVertID].mPairEdge >= parentContour.mEnd)
                {
                    otherPoly.edges()[otherVertID].mPairEdge -= vertCount;
                }
            }

			// Fix up the references to polygons later on in the list from the removed polygon.
            if (otherPoly.edges()[otherVertID].mPairPoly > polyID)
            {
                --otherPoly.edges()[otherVertID].mPairPoly;
            }
        }
    }

    return true;
}

void Mesh::smoothPolygon(int32_t polyID, float smoothing)
{
    Poly& poly = mPolygons[polyID];

	for (ContourVector::iterator it = poly.contours().begin(); it != poly.contours().end(); ++it)
    {
        int32_t i1 = it->mBegin;

        do
        {
			int32_t i0 = poly.prevVert(i1);

			if (!poly.edges()[i1].isImportant())
            {
				int32_t i2 = poly.nextVert(i1);
				int32_t i3 = poly.nextVert(i2);
                Vec2f v0 = poly.vertices()[i0];
                Vec2f v1 = poly.vertices()[i1];
                Vec2f v2 = poly.vertices()[i2];
                Vec2f v3 = poly.vertices()[i3];

				Vec2f e0 = (v1 - v0).normalized();
				Vec2f e2 = (v3 - v2).normalized();

				// If the angle between the previous and next edges is less than 45 degrees then we smooth them.
				// This preserves 90 degree corners pretty effectively.
				if (dot(e0, e2) >= 0.707f)
				{
					v1 = lerp(v1, (v0 + v2) * 0.5f, smoothing);
					moveVert(polyID, i1, v1);
				}
            }

            i1 = i0;
        }
        while (i1 != it->mBegin);
    }
}


void Mesh::posToMap(Vec2f planarPos, int32_t& x, int32_t& y) const
{
    x = (int32_t)(planarPos.x * 0.5f);
    y = (int32_t)(planarPos.y * 0.5f);
}

Vec2f Mesh::mapToPos(int32_t x, int32_t y) const
{
    return Vec2f(x * 2.0f, y * 2.0f);
}

void Mesh::finalizePolygon(int32_t polyID)
{
    Poly& poly = mPolygons[polyID];
    uint32_t vertCount = poly.edges().size();
    poly.allocateVertices();
	poly.setIsIsland(true);

    for (uint32_t thisI0 = 0; thisI0 < vertCount; ++thisI0)
    {
		Edge& edgeI0 = poly.edges()[thisI0];
        poly.vertices()[thisI0] = edgeI0.pos(1.0f);
        edgeI0.mPairEdge = -1;
        edgeI0.mPairPoly = -1;

		if (edgeI0.isImportant())
		{
			poly.setIsIsland(false);
		}

        uint64_t posHash = edgeI0.posHash();
		uint64_t idxHash = (static_cast<uint64_t>(polyID) << 32) | (static_cast<uint64_t>(thisI0) & 0xFFFFFFFF);
		VertMap::iterator it = mPairLookup.find(posHash);

        if (it != mPairLookup.end())
        {
            int32_t thisI1 = poly.nextVert(thisI0);

            for (uint32_t i = 0; i < it->second.size(); ++i)
            {
				uint64_t otherIdxHash = it->second[i];

				if (idxHash != otherIdxHash)
				{
					uint32_t otherPolyID = otherIdxHash >> 32;
					uint32_t otherI1 = otherIdxHash & 0xFFFFFFFFUL;
					Poly& otherPoly = mPolygons[otherPolyID];
					uint32_t otherI0 = otherPoly.prevVert(otherI1);

					if (poly.edges()[thisI1].isCoincident(otherPoly.edges()[otherI0]))
					{
						poly.edges()[thisI0].mPairEdge = otherI0;
						poly.edges()[thisI0].mPairPoly = otherPolyID;
						break;
					}
				}
            }
        }
    }
}

void Mesh::process(const Surface8u& surface)
{
    mMapWidth = surface.getWidth();
    mMapHeight = surface.getHeight();
    mQuadWidth = mMapWidth + 1;
    mQuadHeight = mMapHeight + 1;
    mTotalVertexCount = 0;

	std::unique_ptr< uint8_t[] > visited(new uint8_t[mQuadWidth * mQuadHeight]);
	std::fill(&visited[0], &visited[mQuadWidth * mQuadHeight], 0);

    visited[0] = 7;
    visited[mQuadWidth - 1] = 11;
    visited[(mQuadHeight - 1) * mQuadWidth] = 13;
    visited[mQuadWidth - 1 + (mQuadHeight - 1) * mQuadWidth] = 14;

    for (int32_t x = 1; x < mQuadWidth - 1; ++x)
    {
        visited[x] = 3;
        visited[x + (mQuadHeight - 1) * mQuadWidth] = 12;
    }

    for (int32_t y = 1; y < mQuadHeight - 1; ++y)
    {
        visited[y * mQuadWidth] = 5;

        for (int32_t x = 1; x < mQuadWidth - 1; ++x)
        {
            visited[x + y * mQuadWidth] = 0;
        }

        visited[mQuadWidth - 1 + y * mQuadWidth] = 10;
    }

    // Now that we have the squares defined, iterate through them and create the polygons.
    uint32_t val0, val1, val2, val3;

    for (int32_t y = -1; y < mMapHeight; ++y)
    {
        for (int32_t x = -1; x < mMapWidth; ++x)
        {
            // Grab the working set of the four pair verts.
            getQuad(surface, x, y, val0, val1, val2, val3);

            // If there is no gradient through the quad then early-out.
            if (val0 != val1 || val1 != val2 || val2 != val3)
            {
                int quadIdx = (x + 1) + (y + 1) * mQuadWidth;

                if ((visited[quadIdx] & 1) == 0) traceContour(surface, &visited[0], x, y, val0, 0);
                if ((visited[quadIdx] & 2) == 0) traceContour(surface, &visited[0], x, y, val1, 1);
                if ((visited[quadIdx] & 4) == 0) traceContour(surface, &visited[0], x, y, val2, 2);
                if ((visited[quadIdx] & 8) == 0) traceContour(surface, &visited[0], x, y, val3, 3);
            }
        }
    }

    // Once all the contours have been traced, convert the fixed-point vertices over to
    // floating point and set the half-edge pair indices.
    for (uint32_t i = 0; i < mPolygons.size(); ++i)
    {
        finalizePolygon(i);
    }

    mInitialised = true;
}

}
