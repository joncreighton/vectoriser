#include "vectoriser/poly.h"
#include "cinder/Shape2d.h"

namespace Vectoriser
{
Poly::Poly(uint32_t val, int32_t startX, int32_t startY, int32_t startCell)
	: mValue(val)
	, mStartX(startX)
	, mStartY(startY)
	, mStartCell(startCell)
	, mIsIsland(false)
{
	mContours.push_back(Contour());
	mEdges.reserve(4);
}


bool Poly::testInside(const Vec2f& pos) const
{
    bool inside = false;
	ContourVector::const_iterator it = mContours.begin();

    if (testInsideContour(*it, pos))
    {
        inside = true;
		++it;

		while (it != mContours.end())
        {
            if (testInsideContour(*it, pos))
            {
                inside = false;
                break;
            }

			++it;
        }
    }

    return inside;
}

bool Poly::testInsideContour(const Contour& contour, const Vec2f& pos) const
{
    bool inside = false;

    if (contour.mBounds.testInside(pos))
    {
        int32_t beginVert = contour.mBegin;
        int32_t endVert = contour.mEnd;

        if (endVert - beginVert > 0)
        {
            for (int32_t v1 = beginVert, v0 = endVert - 1; v1 < endVert; v0 = v1++)
            {
                float x0 = mVertices[v0].x;
                float y0 = mVertices[v0].y;
                float x1 = mVertices[v1].x;
                float y1 = mVertices[v1].y;
                float dx = x1 - x0;
                float dy = y1 - y0;

                // From http://local.wasp.uwa.edu.au/~pbourke/geometry/insidepoly/

                if (((y0 <= pos.y) && (pos.y < y1)) || ((y1 <= pos.y) && (pos.y < y0)))
                {
                    if ((pos.x - x1) < dx * (pos.y - y1) / dy)
                    {
                        inside = !inside;
                    }
                }
            }
        }
    }

    return inside;
}

void Poly::nearestEdge(const Vec2f& pos, int32_t& nearestVert, float& minDistanceToVert, int32_t& nearestEdge, float& minDistanceToEdge) const
{
    nearestVert = -1;
    minDistanceToVert = Geo::K_FLOAT_MAX;
    nearestEdge = -1;
    minDistanceToEdge = Geo::K_FLOAT_MAX;

    for (ContourVector::const_iterator it = mContours.begin(); it != mContours.end(); ++it)
    {
        int32_t currVert = it->mBegin;

        for (int32_t i = 0; i < it->mEnd; ++i)
        {
            float distanceToVert = (mVertices[i] - pos).lengthSquared();

            if (distanceToVert < minDistanceToVert)
            {
                minDistanceToVert = distanceToVert;
                nearestVert = i;
            }
        }
    }

    Vec2f prevPos = mVertices[prevVert(nearestVert)];
    Vec2f nearPos = mVertices[nearestVert];
    Vec2f nextPos = mVertices[nextVert(nearestVert)];

    Geo::LineSegment prevEdge(prevPos, nearPos);
    Geo::LineSegment currEdge(nearPos, nextPos);

    float prevEdgeDist = prevEdge.distanceSquared(pos);
    float currEdgeDist = currEdge.distanceSquared(pos);

    if (prevEdgeDist < currEdgeDist)
    {
        nearestEdge = prevVert(nearestVert);
        minDistanceToEdge = prevEdgeDist;
    }
    else
    {
        nearestEdge = nearestVert;
        minDistanceToEdge = currEdgeDist;
    }

    minDistanceToVert = sqrtf(minDistanceToVert);
    minDistanceToEdge = sqrtf(minDistanceToEdge);
}

Geo::IntersectionType Poly::testIntersection(const Geo::Ray& ray, int32_t& edgeIdx, float& distance) const
{
    distance = Geo::K_FLOAT_MAX;
    edgeIdx = -1;
    Geo::IntersectionType intersection = Geo::IT_None;

    for (ContourVector::const_iterator it = mContours.begin(); it != mContours.end(); ++it)
    {
        float currDist = 0.0f;

        if (it->mBounds.testIntersection(ray, currDist) == Geo::IT_Intersect)
        {
            if (currDist < distance)
            {
                for (int32_t vertIdx = it->mBegin, vertEnd = it->mEnd; vertIdx < vertEnd; ++vertIdx)
                {
                    if (ray.testIntersection(edgeLine(vertIdx), currDist) == Geo::IT_Intersect)
                    {
                        if (currDist < distance)
                        {
                            distance = currDist;
                            intersection = Geo::IT_Intersect;
                            edgeIdx = vertIdx;
                        }
                    }
                }
            }
        }
        else
        {
            // If we didn't intersect with the outside contour then exit.
            if (it == mContours.begin()) break;
        }
    }

    return intersection;
}

Geo::IntersectionType Poly::testIntersection(const Geo::LineSegment& line, int32_t& edgeIdx, float& distance) const
{
    float minT = Geo::K_FLOAT_MIN;
    distance = Geo::K_FLOAT_MAX;
    edgeIdx = -1;
    Geo::IntersectionType intersection = Geo::IT_None;

    for (ContourVector::const_iterator it = mContours.begin(); it != mContours.end(); ++it)
    {
        float t = 0.0f;

        if (it->mBounds.testIntersection(line, t) != Geo::IT_None)
        {
            if (t < minT)
            {
                for (int32_t vertIdx = it->mBegin, vertEnd = it->mEnd; vertIdx < vertEnd; ++vertIdx)
                {
                    if (line.testIntersection(edgeLine(vertIdx), t) == Geo::IT_Intersect)
                    {
                        if (t < minT)
                        {
                            minT = t;
                            intersection = Geo::IT_Intersect;
                            edgeIdx = vertIdx;
                        }
                    }
                }
            }
        }
        else
        {
            // If we didn't intersect with the outside contour then exit.
            if (it == mContours.begin()) break;
        }
    }

    distance = line.length() * minT;

    return intersection;
}

void Poly::convertToShape(Shape2d& shape) const
{
	shape.clear();

	for (ContourVector::const_iterator it = mContours.begin(); it != mContours.end(); ++it)
	{
		Path2d contour;

		std::vector<Vec2f>& points = contour.getPoints();
		std::vector<Path2d::SegmentType>& segments = contour.getSegments();

		points.reserve(it->mEnd - it->mBegin);
		segments.reserve(it->mEnd - it->mBegin);
		
		for (int32_t i = it->mBegin; i < it->mEnd; ++i)
		{
			points.push_back(mVertices[i]);
			segments.push_back(Path2d::LINETO);
		}

		segments.back() = Path2d::CLOSE;

		shape.appendContour(contour);
	}
}

}